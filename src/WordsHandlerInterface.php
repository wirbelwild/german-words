<?php

/**
 * Bit&Black German words.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack;

/**
 * Interface WordsHandlerInterface
 *
 * @package BitAndBlack
 */
interface WordsHandlerInterface
{
    /**
     * @param Word $word
     * @return mixed
     */
    public function add(Word $word);

    /**
     * @param string $word
     * @param bool $ignoreCase
     * @return Word
     */
    public function get(string $word, bool $ignoreCase = false): Word;

    /**
     * @param string $word
     * @param bool $ignoreCase
     * @return bool
     */
    public function has(string $word, bool $ignoreCase = false): bool;
}
