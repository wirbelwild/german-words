<?php

/**
 * Bit&Black German words.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack;

use BitAndBlack\Cache\CacheInterface;
use BitAndBlack\Cache\NullCache;
use BitAndBlack\Exception\WordNotFoundException;
use BitAndBlack\File\FileInterface;
use BitAndBlack\IgnoredWords\IgnoredWordsInterface;
use BitAndBlack\IgnoredWords\NullIgnoredWords;

/**
 * Class Words
 *
 * @package BitAndBlack
 * @see \BitAndBlack\Tests\WordsTest
 */
class Words implements WordsHandlerInterface
{
    /**
     * @var array<string, Word>
     */
    private $words = [];
    
    /**
     * @var CacheInterface
     */
    private $cache;
    
    /**
     * @var FileInterface
     */
    private $file;

    /**
     * @var bool
     */
    private $hasLoadedFullData = false;

    /**
     * @var IgnoredWordsInterface
     */
    private $ignoredWords;

    /**
     * @var Word[]
     */
    private $wordsLowercased = [];

    /**
     * Words constructor.
     *
     * @param FileInterface $file
     * @param CacheInterface|null $cache
     * @param IgnoredWordsInterface|null $ignoredWords
     */
    public function __construct(
        FileInterface $file,
        CacheInterface $cache = null,
        IgnoredWordsInterface $ignoredWords = null
    ) {
        $this->file = $file;
        $this->cache = $cache ?? new NullCache();
        $this->ignoredWords = $ignoredWords ?? new NullIgnoredWords();
    }

    /**
     * @param Word $word
     * @return \BitAndBlack\Words
     */
    public function add(Word $word): self
    {
        $this->words[$word->getLemma()] = $word;
        return $this;
    }

    /**
     * @param string $word
     * @param bool $ignoreCase
     * @return Word
     * @throws WordNotFoundException
     */
    public function get(string $word, bool $ignoreCase = false): Word
    {
        if ($this->ignoredWords->has($word, $ignoreCase)) {
            throw new WordNotFoundException($word);
        }
        
        $fromCache = $this->cache->has($word, $ignoreCase);
        
        if (!$fromCache && !$this->hasLoadedFullData) {
            $this->loadData();
        }
        
        $fromLocal = $this->has($word, $ignoreCase);
        
        if (!$fromCache && !$fromLocal) {
            $this->ignoredWords->add($word);
            throw new WordNotFoundException($word);
        }
        
        if ($fromCache) {
            return $this->cache->get($word, $ignoreCase);
        }
        
        $match = false === $ignoreCase
            ? $this->words[$word]
            : $this->wordsLowercased[$word]
        ;
        
        $this->cache->add($match);
        
        return $match;
    }

    /**
     * @param string $word
     * @param bool $ignoreCase
     * @return bool
     */
    public function has(string $word, bool $ignoreCase = false): bool
    {
        if ($this->ignoredWords->has($word, $ignoreCase)) {
            return false;
        }

        $fromCache = $this->cache->has($word, $ignoreCase);

        if (!$fromCache && !$this->hasLoadedFullData) {
            $this->loadData();
        }

        $hasWord = false;

        if (true === $ignoreCase) {
            $word = mb_strtolower($word);
            
            if ([] === $this->wordsLowercased) {
                $this->lowerCaseWords();
            }

            $hasWord = array_key_exists($word, $this->wordsLowercased);
        }
        
        if (false === $ignoreCase) {
            $fromLocal = isset($this->words[$word]);
            $hasWord = $fromCache || $fromLocal;
        }
        
        if (false === $hasWord) {
            $this->ignoredWords->add($word);
        }
        
        return $hasWord;
    }

    /**
     * Loads the whole dataset
     *
     * @return bool
     */
    private function loadData(): bool
    {
        foreach ($this->file->getRecords() as $record) {
            if (isset($record['pos']) && 'Substantiv' !== $record['pos']) {
                continue;
            }

            $this->add(
                new Word($record)
            );
        }
        
        return $this->hasLoadedFullData = true;
    }

    /**
     * Returns the whole dataset of words.
     *
     * @return Word[]
     */
    public function getAllWords(): array
    {
        if (!$this->hasLoadedFullData) {
            $this->loadData();
        }
        
        return $this->words;
    }

    /**
     * Lowercases all words to find them case-insensitive.
     */
    private function lowerCaseWords(): void
    {
        foreach ($this->words as $key => $wordTemp) {
            $keyLowerCased = mb_strtolower($key);
            $this->wordsLowercased[$keyLowerCased] = $wordTemp;
        }
    }

    /**
     * This method searches for a word and can find it in all of its variations.
     * For example, if the word would be `Lösungen`, which is the plural of `Lösung`,
     * this method will find it anyway.
     *
     * @throws WordNotFoundException
     */
    public function findWord(string $wordToFind, bool $ignoreCase = false): ?Word
    {
        if ($this->has($wordToFind)) {
            return $this->get($wordToFind);
        }

        foreach ($this->getAllWords() as $word) {
            $extracted = $word->extract();

            if (in_array($wordToFind, $extracted, true)) {
                return $word;
            }

            if (false === $ignoreCase) {
                continue;
            }

            foreach ($extracted as $wordPossibilities) {
                if (null === $wordPossibilities) {
                    continue;
                }

                $caseInsensitiveMatch = mb_strtolower($wordPossibilities) === mb_strtolower($wordToFind);

                if (true === $caseInsensitiveMatch) {
                    return $word;
                }
            }
        }

        return null;
    }
}
