<?php

/**
 * Bit&Black German words.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\IgnoredWords;

/**
 * Class NullIgnoredWords
 *
 * @package BitAndBlack\IgnoredWords
 */
class NullIgnoredWords implements IgnoredWordsInterface
{
    /**
     * @param string $word
     * @return \BitAndBlack\IgnoredWords\NullIgnoredWords
     */
    public function add(string $word): self
    {
        return $this;
    }

    /**
     * @param string $word
     * @param bool $ignoreCase
     * @return bool
     */
    public function has(string $word, bool $ignoreCase = false): bool
    {
        return false;
    }
}
