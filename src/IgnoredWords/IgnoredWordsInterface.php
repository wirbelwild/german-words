<?php

/**
 * Bit&Black German words.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\IgnoredWords;

/**
 * Interface IgnoredWordsInterface
 *
 * @package BitAndBlack\IgnoredWords
 */
interface IgnoredWordsInterface
{
    /**
     * @param string $word
     * @return mixed
     */
    public function add(string $word);

    /**
     * @param string $word
     * @param bool $ignoreCase
     * @return bool
     */
    public function has(string $word, bool $ignoreCase = false): bool;
}
