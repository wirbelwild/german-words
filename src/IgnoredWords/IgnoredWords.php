<?php

/**
 * Bit&Black German words.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\IgnoredWords;

use BitAndBlack\File\FileInterface;
use BitAndBlack\Word;

/**
 * Class IgnoredWords
 *
 * @package BitAndBlack\IgnoredWords
 */
class IgnoredWords implements IgnoredWordsInterface
{
    /**
     * @var Word[]
     */
    private $words = [];
    
    /**
     * IgnoredWords constructor.
     *
     * @param FileInterface $file
     */
    public function __construct(FileInterface $file)
    {
        foreach ($file->getRecords() as $word) {
            $this->words[$word['lemma']] = new Word($word);
        }
        
        register_shutdown_function(function () use ($file): void {
            $file->save($this->words);
        });
    }

    /**
     * @param string $word
     * @return \BitAndBlack\IgnoredWords\IgnoredWords
     */
    public function add(string $word): self
    {
        $this->words[$word] = new Word([
            'lemma' => $word,
        ]);
        
        return $this;
    }

    /**
     * @param string $word
     * @param bool $ignoreCase
     * @return bool
     */
    public function has(string $word, bool $ignoreCase = false): bool
    {
        if ($ignoreCase) {
            $word = mb_strtolower($word);
            $wordsTemp = array_map(
                static function (string $wordExisting) {
                    return mb_strtolower($wordExisting);
                },
                $this->words
            );
            return isset($wordsTemp[$word]);
        }

        return isset($this->words[$word]);
    }
}
