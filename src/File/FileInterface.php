<?php

/**
 * Bit&Black German words.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\File;

use BitAndBlack\Word;
use Iterator;

/**
 * Interface FileInterface
 *
 * @package BitAndBlack\Loader
 */
interface FileInterface
{
    /**
     * Returns all loaded records
     *
     * @return Iterator<array>
     */
    public function getRecords(): Iterator;

    /**
     * @param array<Word> $words
     * @return bool
     */
    public function save(array $words): bool;
}
