<?php

/**
 * Bit&Black German words.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\File;

use BitAndBlack\Word;
use Iterator;
use League\Csv\CannotInsertRecord;
use League\Csv\Exception;
use League\Csv\Reader;
use League\Csv\UnavailableStream;
use League\Csv\Writer;

/**
 * Class CSV
 *
 * @package BitAndBlack\Loader
 */
class CSV implements FileInterface
{
    /**
     * @var Iterator<array>
     */
    private $records;
    
    /**
     * @var string
     */
    private $file;

    /**
     * CSV constructor.
     *
     * @param string $file
     * @param int|null $header
     * @throws Exception
     */
    public function __construct(string $file, int $header = null)
    {
        if (!is_file($file) || !file_exists($file)) {
            file_put_contents($file, '');
            $header = null;
        }
        
        $reader = Reader::createFromPath($file);
        
        if (null !== $header) {
            $reader->setHeaderOffset($header);
        }
        
        $this->file = $file;
        $this->records = $reader->getRecords();
    }

    /**
     * Destructor
     */
    public function __destruct()
    {
        if ('' === file_get_contents($this->file)) {
            unlink($this->file);
        }
    }

    /**
     * @return Iterator<array>
     */
    public function getRecords(): Iterator
    {
        return $this->records;
    }

    /**
     * @param array<Word> $words
     * @return bool
     * @throws CannotInsertRecord
     * @throws Exception
     * @throws UnavailableStream
     */
    public function save(array $words): bool
    {
        $writer = Writer::createFromPath($this->file);
        $success = 0;

        $words = array_values($words);
        
        if (empty($words)) {
            return false;
        }

        /** @var Word $firstWord */
        $firstWord = $words[0];
        $firstWordKeys = array_keys($firstWord->extract());
        
        $writer->insertOne($firstWordKeys);

        /**
         * @var Word $word
         */
        foreach ($words as $word) {
            $success &= $writer->insertOne($word->extract());
        }
        
        return (bool) $success;
    }
}
