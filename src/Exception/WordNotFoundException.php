<?php

/**
 * Bit&Black German words.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Exception;

use BitAndBlack\Exception;

/**
 * Class WordNotFoundException
 *
 * @package BitAndBlack\Exception
 */
class WordNotFoundException extends Exception
{
    /**
     * WordNotFoundException constructor.
     *
     * @param string $word
     */
    public function __construct(string $word)
    {
        parent::__construct('Couldn\'t find word ' . $word);
    }
}
