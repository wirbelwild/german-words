<?php

/**
 * Bit&Black German words.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Cache;

use BitAndBlack\Exception\WordNotFoundException;
use BitAndBlack\File\FileInterface;
use BitAndBlack\Word;

/**
 * Class Cache
 *
 * @package BitAndBlack
 */
class Cache implements CacheInterface
{
    /**
     * @var Word[]
     */
    private $words = [];

    /**
     * Cache constructor.
     *
     * @param FileInterface $file
     */
    public function __construct(FileInterface $file)
    {
        foreach ($file->getRecords() as $record) {
            if (isset($record['pos']) && 'Substantiv' !== $record['pos']) {
                continue;
            }

            $this->add(
                new Word($record)
            );
        }
        
        register_shutdown_function(function () use ($file): void {
            if (!empty($this->words)) {
                $file->save($this->words);
            }
        });
    }

    /**
     * @param Word $word
     * @return Cache
     */
    public function add(Word $word): self
    {
        $this->words[$word->getLemma()] = $word;
        return $this;
    }

    /**
     * @param string $word
     * @param bool $ignoreCase
     * @return bool
     */
    public function has(string $word, bool $ignoreCase = false): bool
    {
        if ($ignoreCase) {
            $word = mb_strtolower($word);
            $wordsTemp = array_map(
                static function (string $wordExisting) {
                    return mb_strtolower($wordExisting);
                },
                $this->words
            );
            return isset($wordsTemp[$word]);
        }
        
        return isset($this->words[$word]);
    }

    /**
     * @param string $word
     * @param bool $ignoreCase
     * @return Word
     * @throws WordNotFoundException
     */
    public function get(string $word, bool $ignoreCase = false): Word
    {
        if (!$this->has($word, $ignoreCase)) {
            throw new WordNotFoundException($word);
        }
        
        return $this->words[$word];
    }
}
