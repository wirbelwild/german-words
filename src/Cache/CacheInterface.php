<?php

/**
 * Bit&Black German words.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Cache;

use BitAndBlack\WordsHandlerInterface;

/**
 * Interface CacheInterface
 *
 * @package BitAndBlack
 */
interface CacheInterface extends WordsHandlerInterface
{
}
