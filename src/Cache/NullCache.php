<?php

/**
 * Bit&Black German words.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Cache;

use BitAndBlack\Word;

/**
 * Class NullCache
 *
 * @package BitAndBlack
 */
class NullCache implements CacheInterface
{
    /**
     * @param Word $word
     * @return mixed
     */
    public function add(Word $word)
    {
        return null;
    }

    /**
     * @param string $word
     * @param bool $ignoreCase
     * @return Word
     */
    public function get(string $word, bool $ignoreCase = false): Word
    {
        return new Word([]);
    }

    /**
     * @param string $word
     * @param bool $ignoreCase
     * @return bool
     */
    public function has(string $word, bool $ignoreCase = false): bool
    {
        return false;
    }
}
