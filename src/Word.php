<?php

/**
 * Bit&Black German words.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack;

/**
 * Class Word
 *
 * @package BitAndBlack
 * @see \BitAndBlack\Tests\WordTest
 */
class Word
{
    /**
     * @var array<string, string|null>
     */
    private $word;

    /**
     * Word constructor.
     *
     * @param array<string, string> $word
     */
    public function __construct(array $word)
    {
        $this->word = array_map(
            static function ($part) {
                return '' === $part ? null : $part;
            },
            $word
        );
    }

    /**
     * @return array<string, string|null>
     */
    public function extract(): array
    {
        return $this->word;
    }

    /**
     * @return string
     */
    public function getGenus(): string
    {
        return $this->word['genus']
            ?? $this->word['genus 1']
            ?? $this->word['genus 2']
        ;
    }

    /**
     * @param string $masculine
     * @param string $feminine
     * @param string $neuter
     * @return string
     */
    public function handleGenus(string $masculine, string $feminine, string $neuter): string
    {
        switch ($this->getGenus()) {
            case 'm': return $masculine;
            case 'f': return $feminine;
            case 'n': return $neuter;
        }
        
        return '';
    }

    /**
     * @return string
     */
    public function getLemma(): string
    {
        return $this->word['lemma'];
    }

    /**
     * @param bool $includeWord
     * @return string
     */
    public function getSingular(bool $includeWord = false): string
    {
        $output = $this->handleGenus('der', 'die', 'das');

        if ($includeWord) {
            $word = $this->word['nominativ singular']
                ?? $this->word['nominativ singular 1']
                ?? $this->word['nominativ singular 2']
            ;
            return $output . ' ' . $word;
        }

        return $output;
    }

    /**
     * Gets the plural and the pluralized word
     *
     * @param bool $includeWord
     * @return string
     */
    public function getPlural(bool $includeWord = false): string
    {
        $output = $this->handleGenus('die', 'die', 'die');

        if ($includeWord) {
            $word = $this->getPluralized();
            return $output . ' ' . $word;
        }

        return $output;
    }

    /**
     * Gets the pluralized word
     *
     * @return string
     */
    public function getPluralized(): string
    {
        return $this->word['nominativ plural']
            ?? $this->word['nominativ plural 1']
            ?? $this->word['nominativ plural 2']
        ;
    }
    
    /**
     * @param bool $includeWord
     * @return string
     */
    public function getIndefinite(bool $includeWord = false): string
    {
        $output = $this->handleGenus('ein', 'eine', 'ein');

        if ($includeWord) {
            $word = $this->word['nominativ singular']
                ?? $this->word['nominativ singular 1']
                ?? $this->word['nominativ singular 2']
            ;
            return $output . ' ' . $word;
        }

        return $output;
    }

    /**
     * @param bool $includeWord
     * @return string
     */
    public function getGenitiveSingular(bool $includeWord = false): string
    {
        $output = $this->handleGenus('des', 'der', 'des');

        if ($includeWord) {
            $word = $this->word['genitiv singular']
                ?? $this->word['genitiv singular 1']
                ?? $this->word['genitiv singular 2']
            ;
            return $output . ' ' . $word;
        }

        return $output;
    }

    /**
     * @param bool $includeWord
     * @return string
     */
    public function getGenitivePlural(bool $includeWord = false): string
    {
        $output = $this->handleGenus('der', 'der', 'der');

        if ($includeWord) {
            $word = $this->word['genitiv plural']
                ?? $this->word['genitiv plural 1']
                ?? $this->word['genitiv plural 2']
            ;
            return $output . ' ' . $word;
        }

        return $output;
    }

    /**
     * @param bool $includeWord
     * @return string
     */
    public function getGenitiveIndefinite(bool $includeWord = false): string
    {
        $output = $this->handleGenus('eines', 'einer', 'eines');

        if ($includeWord) {
            $word = $this->word['genitiv singular']
                ?? $this->word['genitiv singular 1']
                ?? $this->word['genitiv singular 2']
            ;
            return $output . ' ' . $word;
        }

        return $output;
    }
    
    /**
     * @param bool $includeWord
     * @return string
     */
    public function getDativeSingular(bool $includeWord = false): string
    {
        $output = $this->handleGenus('dem', 'der', 'dem');

        if ($includeWord) {
            $word = $this->word['dativ singular']
                ?? $this->word['dativ singular 1']
                ?? $this->word['dativ singular 2']
            ;
            return $output . ' ' . $word;
        }

        return $output;
    }
    /**
     * @param bool $includeWord
     * @return string
     */
    public function getDativePlural(bool $includeWord = false): string
    {
        $output = $this->handleGenus('den', 'den', 'den');

        if ($includeWord) {
            $word = $this->word['dativ plural']
                ?? $this->word['dativ plural 1']
                ?? $this->word['dativ plural 2']
            ;
            return $output . ' ' . $word;
        }

        return $output;
    }

    /**
     * @param bool $includeWord
     * @return string
     */
    public function getDativeIndefinite(bool $includeWord = false): string
    {
        $output = $this->handleGenus('einem', 'einer', 'einem');

        if ($includeWord) {
            $word = $this->word['dativ singular']
                ?? $this->word['dativ singular 1']
                ?? $this->word['dativ singular 2']
            ;
            return $output . ' ' . $word;
        }

        return $output;
    }

    /**
     * @param bool $includeWord
     * @return string
     */
    public function getAccusativeSingular(bool $includeWord = false): string
    {
        $output = $this->handleGenus('den', 'die', 'das');

        if ($includeWord) {
            $word = $this->word['akkusativ singular']
                ?? $this->word['akkusativ singular 1']
                ?? $this->word['akkusativ singular 2']
            ;
            return $output . ' ' . $word;
        }

        return $output;
    }
    /**
     * @param bool $includeWord
     * @return string
     */
    public function getAccusativePlural(bool $includeWord = false): string
    {
        $output = $this->handleGenus('die', 'die', 'die');

        if ($includeWord) {
            $word = $this->word['akkusativ plural']
                ?? $this->word['akkusativ plural 1']
                ?? $this->word['akkusativ plural 2']
            ;
            return $output . ' ' . $word;
        }

        return $output;
    }
    
    /**
     * @param bool $includeWord
     * @return string
     */
    public function getAccusativeIndefinite(bool $includeWord = false): string
    {
        $output = $this->handleGenus('einen', 'eine', 'ein');

        if ($includeWord) {
            $word = $this->word['akkusativ singular']
                ?? $this->word['akkusativ singular 1']
                ?? $this->word['akkusativ singular 2']
            ;
            return $output . ' ' . $word;
        }

        return $output;
    }

    /**
     * @param string $article
     * @param bool $includeWord
     * @return string
     */
    public function getPossessiveArticleNominativeSingular(string $article, bool $includeWord = false)
    {
        $output = $article . $this->handleGenus('', 'e', '');

        if ($includeWord) {
            $word = $this->word['nominativ singular']
                ?? $this->word['nominativ singular 1']
                ?? $this->word['nominativ singular 2']
            ;
            return $output . ' ' . $word;
        }

        return $output;
    }

    /**
     * @param string $article
     * @param bool $includeWord
     * @return string
     */
    public function getPossessiveArticleNominativePlural(string $article, bool $includeWord = false)
    {
        $output = $article . 'e';

        if ($includeWord) {
            $word = $this->word['nominativ plural']
                ?? $this->word['nominativ plural 1']
                ?? $this->word['nominativ plural 2']
            ;
            return $output . ' ' . $word;
        }

        return $output;
    }

    /**
     * @param string $article
     * @param bool $includeWord
     * @return string
     */
    public function getPossessiveArticleGenitiveSingular(string $article, bool $includeWord = false)
    {
        $output = $article . $this->handleGenus('es', 'er', 'es');

        if ($includeWord) {
            $word = $this->word['genitiv singular']
                ?? $this->word['genitiv singular 1']
                ?? $this->word['genitiv singular 2']
            ;
            return $output . ' ' . $word;
        }

        return $output;
    }

    /**
     * @param string $article
     * @param bool $includeWord
     * @return string
     */
    public function getPossessiveArticleGenitivePlural(string $article, bool $includeWord = false)
    {
        $output = $article . 'er';

        if ($includeWord) {
            $word = $this->word['genitiv plural']
                ?? $this->word['genitiv plural 1']
                ?? $this->word['genitiv plural 2']
            ;
            return $output . ' ' . $word;
        }

        return $output;
    }

    /**
     * @param string $article
     * @param bool $includeWord
     * @return string
     */
    public function getPossessiveArticleAccusativeSingular(string $article, bool $includeWord = false)
    {
        $output = $article . $this->handleGenus('en', 'e', '');

        if ($includeWord) {
            $word = $this->word['akkusativ singular']
                ?? $this->word['akkusativ singular 1']
                ?? $this->word['akkusativ singular 2']
            ;
            return $output . ' ' . $word;
        }

        return $output;
    }

    /**
     * @param string $article
     * @param bool $includeWord
     * @return string
     */
    public function getPossessiveArticleAccusativePlural(string $article, bool $includeWord = false)
    {
        $output = $article . 'e';

        if ($includeWord) {
            $word = $this->word['akkusativ plural']
                ?? $this->word['akkusativ plural 1']
                ?? $this->word['akkusativ plural 2']
            ;
            return $output . ' ' . $word;
        }

        return $output;
    }

    /**
     * @param string $article
     * @param bool $includeWord
     * @return string
     */
    public function getPossessiveArticleDativeSingular(string $article, bool $includeWord = false)
    {
        $output = $article . $this->handleGenus('em', 'er', 'em');

        if ($includeWord) {
            $word = $this->word['dativ singular']
                ?? $this->word['dativ singular 1']
                ?? $this->word['dativ singular 2']
            ;
            return $output . ' ' . $word;
        }

        return $output;
    }

    /**
     * @param string $article
     * @param bool $includeWord
     * @return string
     */
    public function getPossessiveArticleDativePlural(string $article, bool $includeWord = false)
    {
        $output = $article . 'en';

        if ($includeWord) {
            $word = $this->word['dativ plural']
                ?? $this->word['dativ plural 1']
                ?? $this->word['dativ plural 2']
            ;
            return $output . ' ' . $word;
        }

        return $output;
    }
}
