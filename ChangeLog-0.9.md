# Changes in Bit&Black German Words 0.9

## 0.9.0 2023-12-11

### Changed

-   Updated word list with more than 20.000 additional words.
-   Improved handling of uppercase and lowercase words.

### Added

-   Added method `Words::findWord()` to find the source of a word. For example, if the word would be `Lösungen`, which is the plural of `Lösung`, this method will find it anyway.