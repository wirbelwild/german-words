# Changes in Bit&Black German Words 0.8

## 0.8.1 2021-08-12

### Fixed

-   Fixed storage of empty cache files that causes problems when loading again.

## 0.8.0 2020-12-23

### Added

-   Added support for PHP 8.