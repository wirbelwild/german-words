<?php

/**
 * Bit&Black German words.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Tests;

use BitAndBlack\Exception\WordNotFoundException;
use BitAndBlack\File\CSV;
use BitAndBlack\Word;
use BitAndBlack\Words;
use League\Csv\Exception;
use PHPUnit\Framework\TestCase;

/**
 * Class WordsTest.
 *
 * @package BitAndBlack\Tests
 */
class WordsTest extends TestCase
{
    /**
     * @throws Exception
     * @throws WordNotFoundException
     */
    public function testHasAndGet(): void
    {
        $file = __DIR__ . '/data/words.csv';
        $loader = new CSV($file, 0);
        $words = new Words($loader);

        $word1 = 'Lösung';
        $word2 = 'lösung';

        self::assertTrue(
            $words->has($word1)
        );

        self::assertFalse(
            $words->has($word2)
        );

        self::assertTrue(
            $words->has($word2, true)
        );
        
        self::assertInstanceOf(
            Word::class,
            $words->get($word2, true)
        );
    }

    /**
     * @throws WordNotFoundException
     * @throws Exception
     */
    public function testCanFindWord(): void
    {
        $file = __DIR__ . '/data/words.csv';
        $loader = new CSV($file, 0);
        $words = new Words($loader);

        $word1 = 'Lösungen';
        $word2 = 'lösungen';
        
        self::assertInstanceOf(
            Word::class,
            $words->findWord($word1)
        );
        
        self::assertNull(
            $words->findWord($word2)
        );

        self::assertInstanceOf(
            Word::class,
            $words->findWord($word2, true)
        );
    }
}
