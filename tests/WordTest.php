<?php

/**
 * Bit&Black German words.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Tests;

use BitAndBlack\Word;
use PHPUnit\Framework\TestCase;

/**
 * Class WordTest
 *
 * @package BitAndBlack\Tests
 */
class WordTest extends TestCase
{
    /**
     * Tests if the genus can be handled correctly
     */
    public function testCanHandleGenus(): void
    {
        $word = new Word([
            'lemma' => 'Hose',
            'genus' => '',
            'genus 1' => 'f',
            'nominativ singular' => 'Hose',
        ]);

        self::assertSame(
            'f',
            $word->getGenus()
        );

        self::assertSame(
            'die Hose',
            $word->getSingular(true)
        );
    }
}
