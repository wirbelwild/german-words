<?php

use BitAndBlack\Cache\Cache;
use BitAndBlack\Exception\WordNotFoundException;
use BitAndBlack\File\CSV;
use BitAndBlack\IgnoredWords\IgnoredWords;
use BitAndBlack\Words;

ini_set('memory_limit', '-1');

require dirname(__FILE__, 2) . '/vendor/autoload.php';

$file = dirname(__FILE__, 2) . '/data/words.csv';
$fileCached = dirname(__FILE__, 2) . '/data/words-cached.csv';
$fileIgnored = dirname(__FILE__, 2) . '/data/words-ignored.csv';

$loader = new CSV($file, 0);
$loaderCached = new CSV($fileCached, 0);
$loaderIgnored = new CSV($fileIgnored, 0);

$words = new Words(
    $loader,
    new Cache($loaderCached),
    new IgnoredWords($loaderIgnored)
);


$testWords = [
    'Mann',
    'Frau',
    'Kind',
];

$testWords2 = [
    'dein',
    'unser',
    'ihr',
];

echo implode(PHP_EOL, getExampleSentence('Singular:', $testWords, $words, 'getSingular')) . PHP_EOL;
echo implode(PHP_EOL, getExampleSentence('Plural:', $testWords, $words, 'getPlural')) . PHP_EOL;
echo implode(PHP_EOL, getExampleSentence('Unbestimmt:', $testWords, $words, 'getIndefinite')) . PHP_EOL;
echo implode(PHP_EOL, getExampleSentence('Genitiv Singular: Das sind die Freuden', $testWords, $words, 'getGenitiveSingular')) . PHP_EOL;
echo implode(PHP_EOL, getExampleSentence('Genitiv Plural: Das sind die Freuden', $testWords, $words, 'getGenitivePlural')) . PHP_EOL;
echo implode(PHP_EOL, getExampleSentence('Genitiv Unbestimmt: Das sind die Freuden', $testWords, $words, 'getGenitiveIndefinite')) . PHP_EOL;
echo implode(PHP_EOL, getExampleSentence('Dativ Singular: Das gehört', $testWords, $words, 'getDativeSingular')) . PHP_EOL;
echo implode(PHP_EOL, getExampleSentence('Dativ Plural: Das gehört', $testWords, $words, 'getDativePlural')) . PHP_EOL;
echo implode(PHP_EOL, getExampleSentence('Dativ Unbestimmt: Das gehört', $testWords, $words, 'getDativeIndefinite')) . PHP_EOL;
echo implode(PHP_EOL, getExampleSentence('Akkusativ Singular: Frage doch', $testWords, $words, 'getAccusativeSingular')) . PHP_EOL;
echo implode(PHP_EOL, getExampleSentence('Akkusativ Plural: Frage doch', $testWords, $words, 'getAccusativePlural')) . PHP_EOL;
echo implode(PHP_EOL, getExampleSentence('Akkusativ Unbestimmt: Frage doch', $testWords, $words, 'getAccusativeIndefinite')) . PHP_EOL;

echo implode(PHP_EOL, getExampleSentence('Possesiv Nominativ Singular: War das', $testWords, $words, 'getPossessiveArticleNominativeSingular', $testWords2)) . PHP_EOL;
echo implode(PHP_EOL, getExampleSentence('Possesiv Nominativ Plural: Waren das', $testWords, $words, 'getPossessiveArticleNominativePlural', $testWords2)) . PHP_EOL;
echo implode(PHP_EOL, getExampleSentence('Possesiv Genitiv Singular: Das sind die Freuden', $testWords, $words, 'getPossessiveArticleGenitiveSingular', $testWords2)) . PHP_EOL;
echo implode(PHP_EOL, getExampleSentence('Possesiv Genitiv Plural: Das sind die Freuden', $testWords, $words, 'getPossessiveArticleGenitivePlural', $testWords2)) . PHP_EOL;
echo implode(PHP_EOL, getExampleSentence('Possesiv Dativ Singular: Das gehört', $testWords, $words, 'getPossessiveArticleDativeSingular', $testWords2)) . PHP_EOL;
echo implode(PHP_EOL, getExampleSentence('Possesiv Dativ Plural: Das gehört', $testWords, $words, 'getPossessiveArticleDativePlural', $testWords2)) . PHP_EOL;
echo implode(PHP_EOL, getExampleSentence('Possesiv Akkusativ Singular: Das ist für', $testWords, $words, 'getPossessiveArticleAccusativeSingular', $testWords2)) . PHP_EOL;
echo implode(PHP_EOL, getExampleSentence('Possesiv Akkusativ Plural: Das ist für', $testWords, $words, 'getPossessiveArticleAccusativePlural', $testWords2)) . PHP_EOL;

/**
 * @param string $sentence
 * @param array $testWords
 * @param Words $words
 * @param string $method
 * @param array|null $parameter
 * @return array
 * @throws WordNotFoundException
 */
function getExampleSentence(
    string $sentence,
    array $testWords,
    Words $words,
    string $method,
    array $parameter = null
): array {
    $output = [];
    
    if (null !== $parameter) {
        foreach ($parameter as $param) {
            foreach ($testWords as $word) {
                $output[] = $sentence . ' ' . $words->get($word)->$method($param, true);
            }
        }

        return $output;
    }
    
    foreach ($testWords as $word) {
        $output[] = $sentence . ' ' . $words->get($word)->$method(true);
    }

    return $output;
}
